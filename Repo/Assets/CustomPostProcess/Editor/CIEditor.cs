﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class CIEditor
{
	static string[] SCENES = FindEnabledEditorScenes();

	static string APP_NAME = PlayerSettings.productName;
	static string TARGET_DIR = "JServerBuild";

	[MenuItem ("Custom/CI/Build iOS")]
	static void PerformiOSBuild ()
	{
		CreateFolder ();
		string target_dir = APP_NAME;
		GenericBuild (SCENES, TARGET_DIR + "/" + target_dir, BuildTarget.iOS, BuildOptions.None);
	}

	static void CreateFolder ()
	{
		string _datapath = Application.dataPath.Replace ("Assets", "");

		if (!Directory.Exists(_datapath + TARGET_DIR))
			Directory.CreateDirectory (_datapath + TARGET_DIR);
	}

	private static string[] FindEnabledEditorScenes ()
	{
		List<string> EditorScenes = new List<string> ();

		foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
		{
			if (!scene.enabled)
				continue;
			EditorScenes.Add (scene.path);
		}
		return EditorScenes.ToArray ();
	}

	static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options)
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget (build_target);
		string res = BuildPipeline.BuildPlayer (scenes, target_dir, build_target, build_options);
		if(res.Length > 0)
		{
			throw new UnityException ("BuildPlayer Failure: " + res);
		}
	}
}
