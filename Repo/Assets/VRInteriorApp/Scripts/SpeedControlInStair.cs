﻿using UnityEngine;
using System.Collections;

public class SpeedControlInStair : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	void OnTriggerEnter(Collider others)
	{
		if (others.tag == ("Player"))
		{
			Debug.Log (others.name + " Trigger Enter");
			if (others.GetComponent<OVRPlayerController> ())
				others.GetComponent<OVRPlayerController> ().Acceleration /= 2f;
		}
	}

	void OnTriggerExit(Collider others)
	{
		if (others.tag == ("Player"))
		{
			Debug.Log (others.name + "Trigger Exit");
			if (others.GetComponent<OVRPlayerController> ())
				others.GetComponent<OVRPlayerController> ().Acceleration *= 2f;
		}
	}
}
