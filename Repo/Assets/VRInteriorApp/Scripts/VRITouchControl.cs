﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using InteriorApp;
using UnityEngine.EventSystems;

public class VRITouchControl : MonoBehaviour
{
	public OVRScreenFade fadeScreen;
	public HouseMap miniMap;
	public Transform player;

	[Header ("VERTICAL SCROLL CONTENT")]
	public RectTransform[] panelVerticalScrolItem;

	[Header ("VERTICAL SCROLL POSITION")]
	public Vector3 verticalTopPosition;
	public Vector3 verticalCenterPoistion;
	public Vector3 verticalBottomPosition;

	[Header ("SCROLL CONTENT")]
	public RectTransform[] panelScrolItem;

	[Header ("UI SCROLL POSITION")]
	public Vector3 leftPosition;
	public Vector3 centerPosition;
	public Vector3 rightPosition;
	public Vector3 leftMostPosition;
	public Vector3 rightMostPosition;

	[Header ("MAP UI SCROLL POSITION")]
	public Vector3 centerMapPosition;
	public Vector3 leftMostMapPosition;
	public Vector3 rightMostMapPosition;

	[Header ("TELEPORT POSITION")]
	public Transform masterBedroom;
	public Transform guestBedroom, attachedbathRoom, bathRoom, firstCorridor, groundCorridor, kitchen, launderyRoom, loungeRoom, loungeRoom2, terrace, terraceNavigation;

	public GameObject uiObject;
	public GameObject eventSystem;
	public GameObject gazePointer;

	[SerializeField] private GameObject m_leftObject;
	[SerializeField] private GameObject m_centerObject;
	[SerializeField] private GameObject m_rightObject;

	[SerializeField] private GameObject m_verticalTopObject;
	[SerializeField] private GameObject m_verticalCenterObject;
	[SerializeField] private GameObject m_verticalBottomObject;

	private float m_doubleClickStart = 0;
	private bool m_isSwipe = false;
	private string roomToTeleport = "";

	private bool m_isSwipable = true;
	private bool m_isTeleport = true;

	// Use this for initialization
	void OnEnable ()
	{
		m_isTeleport = true;
		ResetVerticalScrolPanel();
	}

	void Start ()
	{
		#if !UNITY_VR && (UNITY_IOS || UNITY_IPHONE || UNITY_ANDROID || UNITY_WEBGL)
		miniMap.player = player;
		uiObject.GetComponent<Canvas>().worldCamera = SceneManager.Instance.playerMainCamera;
		if(eventSystem.GetComponent<StandaloneInputModule>())
		{
			eventSystem.GetComponent<StandaloneInputModule>().enabled = true;
		}
		if(eventSystem.GetComponent<OVRInputModule>())
		{
			eventSystem.GetComponent<OVRInputModule>().enabled = false;
		}
		if(uiObject.GetComponent<OVRRaycaster>())
		{
			uiObject.GetComponent<OVRRaycaster>().enabled = false;
		}
		uiObject.AddComponent<GraphicRaycaster>();
//		#else
//		miniMap.player = player;
//		uiObject.GetComponent<Canvas>().worldCamera = SceneManager.Instance.playerMainCamera;
//		eventSystem.GetComponent<StandaloneInputModule>().enabled = false;
//		eventSystem.GetComponent<OVRInputModule>().rayTransform = SceneManager.Instance.playerMainCamera.transform;
//		eventSystem.GetComponent<OVRInputModule>().enabled = true;
//		uiObject.GetComponent<OVRRaycaster>().enabled = true;
//
//		gazePointer.GetComponent<OVRGazePointer>().cameraRig = SceneManager.Instance.VRCameraRig;
		#endif


		uiObject.SetActive(false);

		OVRTouchpad.Create();
		OVRTouchpad.TouchHandler += HandleTouchHandler;
	}

	void OnDestroy()
	{
		OVRTouchpad.TouchHandler -= HandleTouchHandler;
	}

	public void ResetScrolPanel ()
	{
		for ( int i = 0; i < panelScrolItem.Length; i++)
		{
			if(panelScrolItem [i].GetComponent<iTween>() != null)
			{
				Destroy(panelScrolItem [i].GetComponent<iTween>());
			}
			panelScrolItem [i].gameObject.SetActive (false);
			panelScrolItem [i].GetComponent<Button> ().enabled = false;
		}

		panelScrolItem [panelScrolItem.Length - 1].anchoredPosition3D = leftPosition;
		m_leftObject = panelScrolItem [panelScrolItem.Length - 1].gameObject;
		m_leftObject.GetComponent<Button> ().enabled = true;
		panelScrolItem [0].anchoredPosition3D = centerPosition;
		m_centerObject = panelScrolItem [0].gameObject;
		m_centerObject.GetComponent<Button> ().enabled = true;
		panelScrolItem [1].anchoredPosition3D = rightPosition;
		m_rightObject = panelScrolItem [1].gameObject;
		m_rightObject.GetComponent<Button> ().enabled = true;

		m_leftObject.SetActive (true);
//		m_leftObject.GetComponent<VRInteriorUIFader>().ChangeColor(0.5f);
		m_leftObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		m_centerObject.SetActive (true);
		m_centerObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		m_rightObject.SetActive (true);
//		m_rightObject.GetComponent<VRInteriorUIFader>().ChangeColor(0.5f);
		m_rightObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);

		m_isSwipable = true;

	}


	public void ResetScrolPanelDuringSwipe ()
	{
		for ( int i = 0; i < panelScrolItem.Length; i++)
		{
			if(panelScrolItem [i].GetComponent<iTween>() != null)
			{
				Destroy(panelScrolItem [i].GetComponent<iTween>());
			}
			panelScrolItem [i].gameObject.SetActive (false);
			panelScrolItem [i].GetComponent<Button> ().enabled = false;
		}

		((RectTransform)m_leftObject.transform).anchoredPosition3D = leftPosition;
		m_leftObject.GetComponent<Button> ().enabled = true;
		((RectTransform)m_centerObject.transform).anchoredPosition3D = centerPosition;
		m_centerObject.GetComponent<Button> ().enabled = true;
		((RectTransform)m_rightObject.transform).anchoredPosition3D = rightPosition;
		m_rightObject.GetComponent<Button> ().enabled = true;

		m_leftObject.SetActive (true);
		//		m_leftObject.GetComponent<VRInteriorUIFader>().ChangeColor(0.5f);
		m_leftObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		m_centerObject.SetActive (true);
		m_centerObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		m_rightObject.SetActive (true);
		//		m_rightObject.GetComponent<VRInteriorUIFader>().ChangeColor(0.5f);
		m_rightObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);

		m_isSwipable = true;

	}

	public void ResetVerticalScrolPanel ()
	{
		for ( int i = 0; i < panelVerticalScrolItem.Length; i++)
		{
			if(panelVerticalScrolItem [i].GetComponent<iTween>() != null)
			{
				Destroy(panelVerticalScrolItem [i].GetComponent<iTween>());
			}
			panelVerticalScrolItem [i].gameObject.SetActive (false);
		}

		panelVerticalScrolItem [panelVerticalScrolItem.Length - 1].anchoredPosition3D = verticalTopPosition;
		m_verticalTopObject = panelVerticalScrolItem [panelVerticalScrolItem.Length - 1].gameObject;
		panelVerticalScrolItem [0].anchoredPosition3D = verticalCenterPoistion;
		m_verticalCenterObject = panelVerticalScrolItem [0].gameObject;
		panelVerticalScrolItem [1].anchoredPosition3D = verticalBottomPosition;
		m_verticalBottomObject = panelVerticalScrolItem [1].gameObject;

		m_verticalTopObject.SetActive (false);
		m_verticalCenterObject.SetActive (true);
		m_verticalBottomObject.SetActive (false);

		m_isSwipable = true;

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") != null)
		{
			panelScrolItem = m_verticalCenterObject.GetComponent<PanelScrollItemList>().panelScrolItem;
			m_isTeleport = m_verticalCenterObject.GetComponent<PanelScrollItemList>().isTeleport;
			ResetScrolPanel();
		}
		else
		{
			m_isTeleport = true;
		}
	}

	float uiActiveTime = 0;
	// Update is called once per frame
	void Update ()
	{

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			this.DoubleClick();
		}

		#if UNITY_EDITOR
		if(m_isSwipable && uiObject.activeInHierarchy == true)
		{

			if (Input.GetKeyDown (KeyCode.O))
			{
				SwipeLeft ();
				m_isSwipable = false;
			}

			if (Input.GetKeyDown (KeyCode.P))
			{
				SwipeRight ();
				m_isSwipable = false;
			}

			if (Input.GetKeyDown (KeyCode.U))
			{
				SwipeUp ();
				m_isSwipable = false;
			}

			if (Input.GetKeyDown (KeyCode.J))
			{
				SwipeDown ();
				m_isSwipable = false;
			}

		}

		if (Input.GetKeyDown (KeyCode.I))
			Navigate("Master_Bedroom");
		
//			NavigationArrowController.instance.AssignTarget (masterBedroom);
		#endif


		#if UNITY_WEBGL
		if(m_isSwipable && uiObject.activeInHierarchy == true)
		{

			if (Input.GetKeyDown (KeyCode.LeftArrow))
			{
				SwipeLeft ();
				m_isSwipable = false;
			}

			if (Input.GetKeyDown (KeyCode.RightArrow))
			{
				SwipeRight ();
				m_isSwipable = false;
			}

			if (Input.GetKeyDown (KeyCode.UpArrow))
			{
				SwipeUp ();
				m_isSwipable = false;
			}

			if (Input.GetKeyDown (KeyCode.DownArrow))
			{
				SwipeDown ();
				m_isSwipable = false;
			}

		}
		#endif

		if (VRInteriorManager.instance.isUIEnable == false && uiObject.activeInHierarchy == true)
		{
			if (uiActiveTime == 0)
			{
				uiActiveTime = Time.time;
				return;
			}

			if (uiActiveTime != 0)
			{
				if (Time.deltaTime < uiActiveTime + 12f)
				{
					uiActiveTime = 0;
//					uiObject.SetActive (false);
//					player.GetComponent<OVRPlayerController>().enabled = true;
				}
			}
		}
		else if (VRInteriorManager.instance.isUIEnable == true)
		{
			if (uiActiveTime != 0)
				uiActiveTime = 0;
		}
	}

	public void DoubleClick ()
	{
		if (uiObject.activeSelf)
		{
			fadeScreen.FadeCamera ();

			FinishTween();

			iTween.Stop();
			uiObject.SetActive (false);
			m_isSwipable = true;

			#if MOBILE_BUILD || UNITY_IOS || UNITY_IPHONE
			player.GetComponent<CharacterController>().enabled = true;
			#elif UNITY_WEBGL
			player.GetComponent<CharacterController>().enabled = true;
			player.GetComponent<MouseLook>().enabled = true;
			fadeScreen.GetCamera().GetComponent<MouseLook>().enabled = true;
			#else
			player.GetComponent<OVRPlayerController>().enabled = true;
			#endif

			GotoPreviousPosition();

			return;
		}
		 

//		ResetVerticalScrolPanel();
		SetMiniMapPosition ();
		VRInteriorManager.instance.EnableObjects ("");

		((RectTransform)miniMap.mapWithoutPositionPanel.transform).anchoredPosition3D = leftMostMapPosition;
		miniMap.mapWithoutPositionPanel.SetActive(false);
		((RectTransform)miniMap.mapWithPositionPanel.transform).anchoredPosition3D = centerMapPosition;
		miniMap.mapWithPositionPanel.SetActive(true);
		miniMap.mapWithPositionPanel.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		miniMap.SetPositionActive(true);


		Teleport ("terrace");
	}


	public void FinishTween()
	{
		for ( int i = 0; i < panelScrolItem.Length; i++)
		{
			if(panelScrolItem [i].GetComponent<iTween>() != null)
			{
				Destroy(panelScrolItem [i].GetComponent<iTween>());
			}
			panelScrolItem [i].gameObject.SetActive (false);
		}

		for ( int i = 0; i < panelVerticalScrolItem.Length; i++)
		{
			if(panelVerticalScrolItem [i].GetComponent<iTween>() != null)
			{
				Destroy(panelVerticalScrolItem [i].GetComponent<iTween>());
			}
			panelVerticalScrolItem [i].gameObject.SetActive (false);
		}

		((RectTransform)m_leftObject.transform).anchoredPosition3D = leftPosition;
		m_leftObject.SetActive(true);
		m_leftObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		((RectTransform)m_centerObject.transform).anchoredPosition3D = centerPosition;
		m_centerObject.SetActive(true);
		m_centerObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		((RectTransform)m_rightObject.transform).anchoredPosition3D = rightPosition;
		m_rightObject.SetActive(true);
		m_rightObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);

		((RectTransform)m_verticalTopObject.transform).anchoredPosition3D = verticalTopPosition;
		m_verticalTopObject.SetActive(false);
		((RectTransform)m_verticalCenterObject.transform).anchoredPosition3D = verticalCenterPoistion;
		m_verticalCenterObject.SetActive(true);
		((RectTransform)m_verticalBottomObject.transform).anchoredPosition3D = verticalBottomPosition;
		m_verticalBottomObject.SetActive(false);


		if(miniMap.mapWithoutPositionPanel.GetComponent<iTween>() != null)
		{
			Destroy(miniMap.mapWithoutPositionPanel.GetComponent<iTween>());
		}
		if(miniMap.mapWithPositionPanel.GetComponent<iTween>() != null)
		{
			Destroy(miniMap.mapWithPositionPanel.GetComponent<iTween>());
		}
		((RectTransform)miniMap.mapWithoutPositionPanel.transform).anchoredPosition3D = leftMostMapPosition;
		miniMap.mapWithoutPositionPanel.SetActive(false);
		((RectTransform)miniMap.mapWithPositionPanel.transform).anchoredPosition3D = centerMapPosition;
		miniMap.mapWithPositionPanel.SetActive(true);
		miniMap.mapWithPositionPanel.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		miniMap.SetPositionActive(true);

	}

	public void OnButtonClick (Button buttonName)
	{
		DoubleTab ("Button Click");
		roomToTeleport = buttonName.name;
	}

	void HandleTouchHandler (object sender, System.EventArgs args)
	{
		var touchArgs = (OVRTouchpad.TouchArgs)args;
		OVRTouchpad.TouchEvent touchEvent = touchArgs.TouchType;

		switch(touchEvent)
		{
		case OVRTouchpad.TouchEvent.SingleTap:
			//Debug.Log("SINGLE CLICK\n");
			if ((Time.time - m_doubleClickStart) < 0.3f) {
				this.DoubleClick ();
				DoubleTab ("DOUBLE CLICK");
				roomToTeleport = "";
				m_doubleClickStart = -1f;
			} else {
				DoubleTab ("SINGLE CLICK");
				m_doubleClickStart = Time.time;

				if (roomToTeleport != "" && m_isSwipe == false)
				{
					m_doubleClickStart = -1f;

					if(m_isTeleport)
					{
						Teleport (roomToTeleport);
					}
					else
					{
						Navigate (roomToTeleport);
					}
					roomToTeleport = "";
				}
			}
			break;

		case OVRTouchpad.TouchEvent.Left:
			//Debug.Log("LEFT SWIPE\n");

			if(m_isSwipable && uiObject.activeInHierarchy == true)
			{
//				if (VRInteriorManager.instance.isUIEnable == false)
//					return;

				DoubleTab ("SWIPE LEFT 3");
				StartCoroutine (Swipe ());
				SwipeLeft ();
				m_isSwipable = false;
			}
			break;

		case OVRTouchpad.TouchEvent.Right:
			//Debug.Log("RIGHT SWIPE\n");

			if(m_isSwipable && uiObject.activeInHierarchy == true)
			{
//				if (VRInteriorManager.instance.isUIEnable == false)
//					return;

				DoubleTab ("SWIPE RIGHT 3");
				StartCoroutine (Swipe ());
				SwipeRight ();
				m_isSwipable = false;
			}
			break;

		case OVRTouchpad.TouchEvent.Up:
			//Debug.Log("UP SWIPE\n");

			if(m_isSwipable && uiObject.activeInHierarchy == true)
			{
				StartCoroutine (Swipe ());
				SwipeUp ();
				m_isSwipable = false;
			}
			break;

		case OVRTouchpad.TouchEvent.Down:
			//Debug.Log("DOWN SWIPE\n");

			if(m_isSwipable && uiObject.activeInHierarchy == true)
			{
				StartCoroutine (Swipe ());
				SwipeDown ();
				m_isSwipable = false;
			}
			break;
		}
	}

	#region SWIPE_REGION

	RectTransform GetNextObject (GameObject gob, bool scrollLeft)
	{
		if (scrollLeft == true)
		{
			for (int i = 0; i < panelScrolItem.Length; i++)
			{
				if ( panelScrolItem[i].gameObject == gob )
				{
					if (i == panelScrolItem.Length - 1)
						return panelScrolItem [0];
					else
						return panelScrolItem [i + 1];
				}
			}
		}
		else
		{
			for (int i = panelScrolItem.Length - 1; i >= 0; i--)
			{
				if ( panelScrolItem[i].gameObject == gob )
				{
					if (i == 0)
						return panelScrolItem [panelScrolItem.Length - 1];
					else
						return panelScrolItem [i - 1];
				}
			}
		}

		return null;
	}


	RectTransform GetNextVerticalObject (GameObject gob, bool scrollUp)
	{
		if (scrollUp == true)
		{
			for (int i = 0; i < panelVerticalScrolItem.Length; i++)
			{
				if ( panelVerticalScrolItem[i].gameObject == gob )
				{
					if (i == panelVerticalScrolItem.Length - 1)
						return panelVerticalScrolItem [0];
					else
						return panelVerticalScrolItem [i + 1];
				}
			}
		}
		else
		{
			for (int i = panelVerticalScrolItem.Length - 1; i >= 0; i--)
			{
				if ( panelVerticalScrolItem[i].gameObject == gob )
				{
					if (i == 0)
						return panelVerticalScrolItem [panelVerticalScrolItem.Length - 1];
					else
						return panelVerticalScrolItem [i - 1];
				}
			}
		}

		return null;
	}

	void SwipeLeft ()
	{
		roomToTeleport = "";

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") == null)
		{
			MoveToLeftMostMap(miniMap.GetActiveMapPanel());

			GameObject tempObject = miniMap.GetInActiveMapPanel();

			((RectTransform)tempObject.transform).anchoredPosition3D = rightMostMapPosition;
			MoveToCenterForMap (tempObject);
			return;
		}

		MoveToLeftMost (m_leftObject);
		m_leftObject = null;

		MoveToLeft (m_centerObject);
		m_leftObject = m_centerObject;
		m_leftObject.GetComponent<Button> ().enabled = true;
		m_centerObject = null;

		MoveToCenter (m_rightObject);
		m_centerObject = m_rightObject;
		m_centerObject.GetComponent<Button> ().enabled = true;
		m_rightObject = null;

		RectTransform rectObject = GetNextObject (m_centerObject, true);
		rectObject.anchoredPosition3D = rightMostPosition;
		MoveToRight (rectObject.gameObject);
		m_rightObject = rectObject.gameObject;
		m_rightObject.GetComponent<Button> ().enabled = true;
	}

	void SwipeRight ()
	{
		roomToTeleport = "";

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") == null)
		{
			MoveToRightMostMap(miniMap.GetActiveMapPanel());

			GameObject tempObject = miniMap.GetInActiveMapPanel();

			((RectTransform)tempObject.transform).anchoredPosition3D = leftMostMapPosition;
			MoveToCenterForMap (tempObject);
			return;
		}

		MoveToRightMost (m_rightObject);
		m_rightObject = null;

		MoveToRight (m_centerObject);
		m_rightObject = m_centerObject;
		m_rightObject.GetComponent<Button> ().enabled = true;
		m_centerObject = null;

		MoveToCenter (m_leftObject);
		m_centerObject = m_leftObject;
		m_centerObject.GetComponent<Button> ().enabled = true;
		m_leftObject = null;

		RectTransform rectObject = GetNextObject (m_centerObject, false);
		rectObject.anchoredPosition3D = leftMostPosition;
		MoveToLeft (rectObject.gameObject);
		m_leftObject = rectObject.gameObject;
		m_leftObject.GetComponent<Button> ().enabled = true;
	}

	void SwipeUp ()
	{
		roomToTeleport = "";
		PanelScrollItemList scrollItemList = new PanelScrollItemList();

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") != null)
		{
			scrollItemList = m_verticalCenterObject.GetComponent<PanelScrollItemList>();

			scrollItemList.centreObject = m_centerObject;
			scrollItemList.leftObject = m_leftObject;
			scrollItemList.rightObject = m_rightObject;
		}

		m_leftObject.GetComponent<Button> ().enabled = false;
		m_centerObject.GetComponent<Button> ().enabled = false;
		m_rightObject.GetComponent<Button> ().enabled = false;

		VerticalMoveToBottom (m_verticalCenterObject);
		m_verticalBottomObject = m_verticalCenterObject;
		m_verticalCenterObject = null;

		VerticalMoveToCentre (m_verticalTopObject);
		m_verticalCenterObject = m_verticalTopObject;
		m_verticalTopObject = null;

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") != null)
		{
			scrollItemList = m_verticalCenterObject.GetComponent<PanelScrollItemList>();

			panelScrolItem = scrollItemList.panelScrolItem;
			m_isTeleport = scrollItemList.isTeleport;

			m_centerObject = scrollItemList.centreObject;
			m_leftObject = scrollItemList.leftObject;
			m_rightObject = scrollItemList.rightObject;

			ResetScrolPanelDuringSwipe();
		}
		else
		{
			m_isTeleport = true;
		}

		RectTransform rectObject = GetNextVerticalObject (m_verticalCenterObject, false);
		rectObject.anchoredPosition3D = verticalTopPosition;
		VerticalMoveToTopMost (rectObject.gameObject);
		m_verticalTopObject = rectObject.gameObject;

	}

	void SwipeDown ()
	{
		roomToTeleport = "";
		PanelScrollItemList scrollItemList = new PanelScrollItemList();

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") != null)
		{
			scrollItemList = m_verticalCenterObject.GetComponent<PanelScrollItemList>();

			scrollItemList.centreObject = m_centerObject;
			scrollItemList.leftObject = m_leftObject;
			scrollItemList.rightObject = m_rightObject;
		}

		m_leftObject.GetComponent<Button> ().enabled = false;
		m_centerObject.GetComponent<Button> ().enabled = false;
		m_rightObject.GetComponent<Button> ().enabled = false;

		VerticalMoveToTop (m_verticalCenterObject);
		m_verticalTopObject = m_verticalCenterObject;
		m_verticalCenterObject = null;

		VerticalMoveToCentre (m_verticalBottomObject);
		m_verticalCenterObject = m_verticalBottomObject;
		m_verticalBottomObject = null;

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") != null)
		{
			scrollItemList = m_verticalCenterObject.GetComponent<PanelScrollItemList>();

			panelScrolItem = scrollItemList.panelScrolItem;
			m_isTeleport = scrollItemList.isTeleport;

			m_centerObject = scrollItemList.centreObject;
			m_leftObject = scrollItemList.leftObject;
			m_rightObject = scrollItemList.rightObject;

			ResetScrolPanelDuringSwipe();
		}
		else
		{
			m_isTeleport = true;
		}

		RectTransform rectObject = GetNextVerticalObject (m_verticalCenterObject, true);
		rectObject.anchoredPosition3D = verticalBottomPosition;
		VerticalMoveToBottomMost (rectObject.gameObject);
		m_verticalBottomObject = rectObject.gameObject;
	}

	IEnumerator Swipe ()
	{
		m_isSwipe = true;
		yield return new WaitForSeconds (0.3f);
		m_isSwipe = false;
	}

	#endregion

	void Teleport (string roomname)
	{
		fadeScreen.FadeCamera ();

		FinishTween();

		iTween.Stop();
		uiObject.SetActive (false);
		m_isSwipable = true;

		#if MOBILE_BUILD || UNITY_IOS || UNITY_IPHONE
		player.GetComponent<CharacterController>().enabled = true;
		#elif UNITY_WEBGL
		player.GetComponent<CharacterController>().enabled = true;
		player.GetComponent<MouseLook>().enabled = true;
		fadeScreen.GetCamera().GetComponent<MouseLook>().enabled = true;
		#else
		player.GetComponent<OVRPlayerController>().enabled = true;
		#endif

		if (roomname.Contains ("Master_Bedroom"))
		{
			player.transform.position = masterBedroom.position;
			player.transform.rotation = masterBedroom.rotation;
		}
		else if (roomname.Contains ("Guest_Bedroom"))
		{
			player.transform.position = guestBedroom.position;
			player.transform.rotation = guestBedroom.rotation;
		}
		else if (roomname.Contains ("Attached_Bathroom"))
		{
			player.transform.position = attachedbathRoom.position;
			player.transform.rotation = attachedbathRoom.rotation;
		}
		else if (roomname.Contains ("Bathroom"))
		{
			player.transform.position = bathRoom.position;
			player.transform.rotation = bathRoom.rotation;
		}
		else if (roomname.Contains ("First_Corridor"))
		{
			player.transform.position = firstCorridor.position;
			player.transform.rotation = firstCorridor.rotation;
		}
		else if (roomname.Contains ("Laundery"))
		{
			player.transform.position = launderyRoom.position;
			player.transform.rotation = launderyRoom.rotation;
		}
		else if (roomname.Contains ("Lounge_1"))
		{
			player.transform.position = loungeRoom.position;
			player.transform.rotation = loungeRoom.rotation;
		}
		else if (roomname.Contains ("Lounge_2"))
		{
			player.transform.position = loungeRoom2.position;
			player.transform.rotation = loungeRoom2.rotation;
		}
		else if (roomname.Contains ("Kitchen"))
		{
			player.transform.position = kitchen.position;
			player.transform.rotation = kitchen.rotation;
		}
		else if (roomname.Contains ("Ground_Corridor"))
		{
			player.transform.position = groundCorridor.position;
			player.transform.rotation = groundCorridor.rotation;
		}
		else if (roomname.Contains ("terrace"))
		{
			iTween.Stop();
			uiObject.SetActive (true);
			m_isSwipable = true;

			#if MOBILE_BUILD || UNITY_IOS || UNITY_IPHONE
			player.GetComponent<CharacterController>().enabled = false;
			fadeScreen.GetCamera().transform.localRotation = Quaternion.Euler(Vector3.zero);
			#elif UNITY_WEBGL
			player.GetComponent<CharacterController>().enabled = false;
			player.GetComponent<MouseLook>().enabled = false;
			fadeScreen.GetCamera().GetComponent<MouseLook>().enabled = false;
			fadeScreen.GetCamera().transform.localRotation = Quaternion.Euler(Vector3.zero);
			#else
			player.GetComponent<OVRPlayerController>().enabled = false;
			#endif
			player.transform.position = terrace.position;
			player.transform.rotation = terrace.rotation;
			NavigationArrowController.instance.RemoveArrow ();
		}

		VRInteriorManager.instance.EnableObjects (roomname);
	}

	void Navigate (string roomname)
	{
		fadeScreen.FadeCamera ();

		FinishTween();

		iTween.Stop();
		uiObject.SetActive (false);
		m_isSwipable = true;

		player.transform.position = terraceNavigation.position;
		player.transform.rotation = terraceNavigation.rotation;

		#if MOBILE_BUILD || UNITY_IOS || UNITY_IPHONE
		player.GetComponent<CharacterController>().enabled = true;
		#elif UNITY_WEBGL
		player.GetComponent<CharacterController>().enabled = true;
		player.GetComponent<MouseLook>().enabled = true;
		fadeScreen.GetCamera().GetComponent<MouseLook>().enabled = true;
		#else
		player.GetComponent<OVRPlayerController>().enabled = true;
		#endif


		if (roomname.Contains ("Master_Bedroom"))
		{
			NavigationArrowController.instance.AssignTarget (masterBedroom);
		}
		else if (roomname.Contains ("Guest_Bedroom"))
		{
			NavigationArrowController.instance.AssignTarget (guestBedroom);
		}
		else if (roomname.Contains ("Attached_Bathroom"))
		{
			NavigationArrowController.instance.AssignTarget (attachedbathRoom);
		}
		else if (roomname.Contains ("Bathroom"))
		{
			NavigationArrowController.instance.AssignTarget (bathRoom);
		}
		else if (roomname.Contains ("First_Corridor"))
		{
			NavigationArrowController.instance.AssignTarget (firstCorridor);
		}
		else if (roomname.Contains ("Laundery"))
		{
			NavigationArrowController.instance.AssignTarget (launderyRoom);
		}
		else if (roomname.Contains ("Lounge_1"))
		{
			NavigationArrowController.instance.AssignTarget (loungeRoom);
		}
		else if (roomname.Contains ("Lounge_2"))
		{
			NavigationArrowController.instance.AssignTarget (loungeRoom2);
		}
		else if (roomname.Contains ("Kitchen"))
		{
			NavigationArrowController.instance.AssignTarget (kitchen);
		}
		else if (roomname.Contains ("Ground_Corridor"))
		{
			NavigationArrowController.instance.AssignTarget (groundCorridor);
		}
//		else if (roomname.Contains ("terrace"))
//		{
//			uiObject.SetActive (true);
//			player.transform.position = terrace.position;
//			player.transform.rotation = terrace.rotation;
//		}
	}

	#region ITWEEN

	void VerticalMoveToTopMost (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = verticalTopPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		iTween.MoveTo (gob, newHash);

//		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();

		gob.SetActive (false);
	}

	void VerticalMoveToBottomMost (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = verticalBottomPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		iTween.MoveTo (gob, newHash);

//		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();

		gob.SetActive (false);
	}
	void VerticalMoveToTop (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = verticalTopPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedVerticalTop";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

//		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}

	void ReachedVerticalTop (GameObject gob)
	{
		iTween.Stop();
		gob.SetActive (false);
		m_isSwipable = true;
	}

	void VerticalMoveToCentre (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = verticalCenterPoistion;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

//		gob.GetComponent<VRInteriorUIFader>().FadeInFull();
	}

	void VerticalMoveToBottom (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = verticalBottomPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedVerticalBottom";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

//		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}

	void ReachedVerticalBottom (GameObject gob)
	{
		iTween.Stop();
		gob.SetActive (false);
		m_isSwipable = true;
	}

	void MoveToCenter (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = centerPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;
		
		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

//		gob.GetComponent<VRInteriorUIFader>().FadeInHalfToFull();
	}

	void MoveToLeftMostMap (GameObject gob)
	{
		miniMap.SetPositionActive(false);

		Hashtable newHash = new Hashtable ();
		newHash ["position"] = leftMostMapPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedLeftMost";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeOutHalfToFull();
		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}

	void MoveToCenterForMap (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = centerMapPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		gob.GetComponent<VRInteriorUIFader>().FadeInFull();
	}

	void MoveToRightMostMap (GameObject gob)
	{
		miniMap.SetPositionActive(false);

		Hashtable newHash = new Hashtable ();
		newHash ["position"] = rightMostMapPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedRightMost";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeOutHalfToFull();
		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}

	void MoveToLeft (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = leftPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		if (gob.activeSelf == true)
		{
//			gob.GetComponent<VRInteriorUIFader>().FadeOutHalf();
		}
		else
		{
			gob.SetActive (true);
//			gob.GetComponent<VRInteriorUIFader>().FadeInHalf();
			gob.GetComponent<VRInteriorUIFader>().FadeInFull();
		}
		
		iTween.MoveTo (gob, newHash);

	}
	
	void MoveToRight (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = rightPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		
		if (gob.activeSelf == true)
		{
//			gob.GetComponent<VRInteriorUIFader>().FadeOutHalf();
		}
		else
		{
			gob.SetActive (true);
//			gob.GetComponent<VRInteriorUIFader>().FadeInHalf();
			gob.GetComponent<VRInteriorUIFader>().FadeInFull();
		}

		iTween.MoveTo (gob, newHash);
	}
	
	void MoveToLeftMost (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = leftMostPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedLeftMost";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;
		
		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

//		gob.GetComponent<VRInteriorUIFader>().FadeOutHalfToFull();
		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}
	
	void ReachedLeftMost (GameObject gob)
	{
		iTween.Stop();
		gob.SetActive (false);
		m_isSwipable = true;
		miniMap.SetPositionActive(true);
	}
	
	void MoveToRightMost (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = rightMostPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedRightMost";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;
		
		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

//		gob.GetComponent<VRInteriorUIFader>().FadeOutHalfToFull();
		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}
	
	void ReachedRightMost (GameObject gob)
	{
		iTween.Stop();
		gob.SetActive (false);
		m_isSwipable = true;
		miniMap.SetPositionActive(true);
	}

	#endregion

	public bool isInFirstFloor = false;
	private Vector3 m_previousPosition;
	private Quaternion m_previousRotation;
	private bool isInFirstFloor_Prev;
	void SetMiniMapPosition ()
	{
		m_previousPosition = player.position;
		m_previousRotation = player.rotation;
		isInFirstFloor_Prev = isInFirstFloor;
		miniMap.SavePreviousPosition (m_previousPosition, isInFirstFloor);
	}

	void GotoPreviousPosition ()
	{
		player.transform.position = m_previousPosition;
		player.transform.rotation = m_previousRotation;

		if (isInFirstFloor_Prev)
			VRInteriorManager.instance.EnableObjects("Master_Bedroom");
		else
			VRInteriorManager.instance.EnableObjects("");
	}
	

	#region TESTING_CODE

	public Text test_tab;
	private string test_string;

	void FadeCamera ()
	{
		Teleport ("Master_Bedroom");
	}

	public void DoubleTab (string tab)
	{
		//		test_string += " "+tab;
		//		test_tab.text = test_string;
	}

	#endregion
}
