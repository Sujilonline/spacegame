﻿using UnityEngine;
using System.Collections;

public class VRInteriorManager : MonoBehaviour
{

	public static VRInteriorManager instance { get; private set; }

	public VRITouchControl vrTouchControl;
	public bool isUIEnable = false;
	public GameObject fireEffects;
	public Transform firstPosition;
	public ObjectsController firstFloorControl;
	public ObjectsController groundFloorControl;

	private void Awake()
	{
		// Only allow one instance at runtime.
		if (instance != null) {
//			enabled = false;
//			DestroyImmediate (this);
//			return;
		}

		instance = this;
	}

	// Use this for initialization
	void Start () {
		#if UNITY_EDITOR
		fireEffects.SetActive(false);
		#else
		fireEffects.SetActive(false);
		#endif
	}

	public void EnableObjects (string roomName)
	{
		switch (roomName)
		{
		case "Master_Bedroom": case "Guest_Bedroom": case "Attached_Bathroom": case "Bathroom": case "First_Corridor": case "terrace":
			firstFloorControl.ObjectControl ();
			break;

		case "Laundery": case "Lounge_1": case "Lounge_2": case "Kitchen": case "Ground_Corridor":
			groundFloorControl.ObjectControl ();
			break;

		default:
			groundFloorControl.ObjectControl ();
			break;
		}
	}
}
