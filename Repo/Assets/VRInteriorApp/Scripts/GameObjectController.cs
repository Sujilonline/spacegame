﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class GameObjectController : MonoBehaviour 
{
	public GameObject vrRoot;
	public bool assignObject = true;

	public GameObject[] Objects;
	public List<GameObject> sceneObjects;

	public void ActiveGameObject(bool activate)
	{
		for (int i = 0; i < sceneObjects.Count; i++)
		{
			if(sceneObjects[i] != null)
			{
				sceneObjects [i].SetActive(activate);
			}
		}
	}

	void OnDrawGizmos() 
	{
		if(assignObject)
		{
			Main();
		}
		assignObject = false;
	}

	public void Main()
	{
		string path = "Assets/XMLs/"+gameObject.name+".txt";

		if (!File.Exists(path))
		{
//			 Create a file to write to.
			using (StreamWriter sw = File.CreateText(path))
			{
				for (int i = 0; i < Objects.Length; i++)
				{
					if(Objects[i] != null)
					{
						sw.WriteLine(Objects [i].name);
					}
				}
			}
		}

		else
		{
			using (StreamReader sr = File.OpenText(path))
			{
				UnityEngine.Transform[] allChildrens = vrRoot.GetComponentsInChildren <UnityEngine.Transform> (true);

				string s = "";
				while ((s = sr.ReadLine()) != null)
				{
					foreach (UnityEngine.Transform child in allChildrens)
					{
						if (child.name == s)
						{
							sceneObjects.Add(child.gameObject);
						}
					}
				}
			}
		}
	}
}
