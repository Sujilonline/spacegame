﻿using UnityEngine;
using System.Collections;
using Utils;

namespace InteriorApp
{
	public class SceneManager : MonoBehaviour
	{
		private static SceneManager m_instance;
		public static SceneManager Instance
		{
			get
			{
				return m_instance;
			}

			private set {}
		}

		[HideInInspector] public References references;
		[HideInInspector] public Transform player;
		[HideInInspector] public Canvas uiJoysticksMobile;
		[HideInInspector] public Camera playerMainCamera;
		[HideInInspector] public OVRCameraRig VRCameraRig;

		[SerializeField]
		private GameObject m_VRGameObject;
		[SerializeField]
		private GameObject m_arrowPrefab;

		[SerializeField]
		private GameObject m_VRInteriorManager;
		[SerializeField]
		private GameObject m_menuUI;
		private VRITouchControl m_vriTouchControl;

		void Awake () 
		{
			#if UNITY_VR
			gameObject.SetActive(false);
			return;
			#endif
		

			if (m_instance == null)
				m_instance = this;
			else
				DestroyImmediate(this);

			DontDestroyOnLoad(this.gameObject);
		}

		// Use this for initialization
		void Start () {

		}
		
		// Update is called once per frame
		void Update () {

			if (references == null)
			{
				GameObject go = Resources.Load ("References") as GameObject;
				go = go.Instantiate ("(Ref)" + go.name, this.gameObject, false, true);
				references = go.GetComponent<References> ();

				return;
			}

			if (player == null)
			{
				GameObject go = references.GetPlayer ();
				player = go.Instantiate ("(Prf)" + go.name, null, false, true).transform;
				//player = go.transform;
				player.GetComponent<NavigationArrowController>().arrowObject = m_arrowPrefab;

				Destroy (m_VRGameObject);

				#if UNITY_VR
				VRCameraRig =  player.GetComponentInChildren<OVRCameraRig> ();
				#endif

				return;
			}

			if (m_VRInteriorManager != null && m_VRInteriorManager.activeInHierarchy == false)
			{
				m_VRInteriorManager.SetActive (true);

				player.CopyTransform (VRInteriorManager.instance.firstPosition);

				return;
			}

			if (playerMainCamera == null)
			{
				playerMainCamera = player.GetComponentInChildren<Camera> ();
				return;
			}

			if (m_vriTouchControl == null)
			{
				GameObject go;

				#if !UNITY_VR && (UNITY_IOS || UNITY_IPHONE || UNITY_ANDROID)
					go = references.mainHUDMOBILE.Instantiate ("(UI)MainUI", this.gameObject, false, true);
				#else
					go = references.mainHUD.Instantiate ("(UI)MainUI", this.gameObject, false, true);
				#endif

				m_vriTouchControl = go.GetComponent<VRITouchControl> ();

				VRInteriorManager.instance.vrTouchControl = m_vriTouchControl;
				m_vriTouchControl.fadeScreen = playerMainCamera.GetComponent<OVRScreenFade> ();
				m_vriTouchControl.player = player;


				VRInteriorManager.instance.EnableObjects ("");

				return;
			}

//			#if !UNITY_VR && (UNITY_IOS || UNITY_ANDROID || UNITY_WEBGL)
			#if (!UNITY_VR && UNITY_IOS && UNITY_ANDROID) || (UNITY_WEBGL)
			if (m_menuUI == null)
			{
				GameObject go = references.menuUI.Instantiate ("(UI)MenuUI", this.gameObject, false, true);
				m_menuUI = go;

				return;
			}
			#endif

			#if !UNITY_VR && (UNITY_IOS || UNITY_IPHONE || UNITY_ANDROID)
			if (uiJoysticksMobile == null)
			{
				GameObject go = references.GetMobileJoySticks();
				go = go.Instantiate ("(Prf)"+go.name, this.gameObject, false, true);
				uiJoysticksMobile = go.GetComponent<Canvas>();
			}
			return;
			#endif
		}
	}	
}