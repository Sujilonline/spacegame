﻿using UnityEngine;
using System.Collections;

public class DoorController : MonoBehaviour {

	enum eDoorState
	{
		Open,
		Close
	}

	private const string PLAYER_TAG = "Player";

	public Animation door;
	public AnimationClip[] animationClip;

	[SerializeField] private eDoorState m_doorState = eDoorState.Open;

	void Awake ()
	{
		m_doorState = eDoorState.Close;
		door.clip = animationClip [1];
		door.Play (animationClip[1].name);
	}

	// Use this for initialization
	void Start () {
		if (door.GetComponent<MeshCollider> () != null)
			door.GetComponent<MeshCollider> ().enabled = false;
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == PLAYER_TAG && m_doorState != eDoorState.Open)
		{
			m_doorState = eDoorState.Open;
			door.clip = animationClip [0];
			door.Play (animationClip[0].name);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == PLAYER_TAG  && m_doorState != eDoorState.Close)
		{
			m_doorState = eDoorState.Close;
			door.clip = animationClip [1];
			door.Play (animationClip[1].name);
		}
	}
}
