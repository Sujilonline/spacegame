﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class VRInteriorUIFader : MonoBehaviour
{
	public float fadeTime = 1f;
	public GameObject fadeGameobject;

	[SerializeField] private List<Image> m_imageComponent;
	[SerializeField] private List<Text> m_textComponent;
	[SerializeField] private List<SpriteRenderer> m_spriteCompoent;

	void Awake ()
	{
		fadeGameobject.transform.GetComponentsInChildren<Image> (m_imageComponent);
		fadeGameobject.transform.GetComponentsInChildren<Text> (m_textComponent);
		fadeGameobject.transform.GetComponentsInChildren<SpriteRenderer> (m_spriteCompoent);
	}

	public void FadeInHalf ()
	{
		StartCoroutine (FadeOutColor (0, 0.5f));
	}

	public void FadeInFull ()
	{
		StartCoroutine (FadeOutColor (0, 1f));
	}

	public void FadeInHalfToFull ()
	{
		StartCoroutine (FadeOutColor (0.5f, 1f));
	}

	public void FadeOutFull ()
	{
		StartCoroutine (FadeOutColor (1f, 0));
	}

	public void FadeOutHalf ()
	{
		StartCoroutine (FadeOutColor (1f, 0.5f));
	}

	public void FadeOutHalfToFull()
	{
		StartCoroutine (FadeOutColor (0.5f, 0f));
	}

	IEnumerator FadeOutColor (float fromAlpha, float toAlpha)
	{
		for (float t = 0; t < fadeTime; t += Time.deltaTime)
		{
			float nt = Mathf.Clamp01 (t / fadeTime);
			nt = Mathf.Sin (nt * Mathf.PI * 0.5f);

			for (int i = 0; i < m_imageComponent.Count; i++)
			{
				Color fromColor = m_imageComponent [i].color;
				Color toColor = m_imageComponent [i].color;
				fromColor.a = fromAlpha;
				toColor.a = toAlpha;

				m_imageComponent [i].color = Color.Lerp (fromColor, toColor, nt);
			}

			for (int i = 0; i < m_textComponent.Count; i++)
			{
				Color fromColor = m_textComponent [i].color;
				Color toColor = m_textComponent [i].color;
				fromColor.a = fromAlpha;
				toColor.a = toAlpha;

				m_textComponent [i].color = Color.Lerp (fromColor, toColor, nt);
			}

			for (int i = 0; i < m_spriteCompoent.Count; i++)
			{
				Color fromColor = m_spriteCompoent [i].color;
				Color toColor = m_spriteCompoent [i].color;
				fromColor.a = fromAlpha;
				toColor.a = toAlpha;

				m_spriteCompoent [i].color = Color.Lerp (fromColor, toColor, nt);
			}

			yield return null;
		}
	}

	public void ChangeColor (float toAlpha)
	{
		for (int i = 0; i < m_imageComponent.Count; i++)
		{
			
			Color toColor = m_imageComponent [i].color;

			toColor.a = toAlpha;

			m_imageComponent [i].color = toColor;
		}

		for (int i = 0; i < m_textComponent.Count; i++)
		{
			Color toColor = m_textComponent [i].color;
			toColor.a = toAlpha;

			m_textComponent [i].color = toColor;
		}

		for (int i = 0; i < m_spriteCompoent.Count; i++)
		{
			Color toColor = m_spriteCompoent [i].color;

			toColor.a = toAlpha;

			m_spriteCompoent [i].color = toColor;
		}
	}
}
