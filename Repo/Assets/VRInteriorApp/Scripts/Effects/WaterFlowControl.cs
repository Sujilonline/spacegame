﻿using UnityEngine;
using System.Collections;

public class WaterFlowControl : MonoBehaviour
{
	public GameObject waterEffect;

	private float triggerdTime = 0f;

	private const string PLAYER_TAG = "Player";

	void Awake ()
	{
		if (waterEffect != null)
			waterEffect.SetActive (false);
	}
	
	void OnTriggerStay (Collider others)
	{
		if (waterEffect == null)
			return;

		triggerdTime = Time.time;

		if (others.tag == PLAYER_TAG && waterEffect.activeInHierarchy == false)
		{
			waterEffect.SetActive (true);
		}
	}

	void Update()
	{
		if(Time.time - triggerdTime > 0.5f)
		{
			waterEffect.SetActive (false);
		}
	}
}
