﻿using UnityEngine;
using System.Collections;

public class UIScaleController : MonoBehaviour 
{

	public void ChangeScale(float prevScale, float newScale)
	{
		StartCoroutine (UpdateScale(prevScale, newScale,Time.time));
	}

	public void StopChangeScale()
	{
		StopCoroutine("UpdateScale");
	}

	float timeTakenForLerp = 0.105f;

	IEnumerator UpdateScale(float prevScale, float newScale,float lerpTimeStartedLerping)
	{
		float timeSinceLepStarted;
		float m_lerpPercentageComplete = 0;

		while(m_lerpPercentageComplete < 1)
		{
			timeSinceLepStarted = Time.time - lerpTimeStartedLerping;
			m_lerpPercentageComplete = timeSinceLepStarted / timeTakenForLerp;

			float tempScale = Mathf.Lerp ( prevScale , newScale , m_lerpPercentageComplete);

			((RectTransform)transform).localScale = new Vector3(tempScale, tempScale, tempScale);

			yield return new WaitForEndOfFrame();
		}
	}
}
