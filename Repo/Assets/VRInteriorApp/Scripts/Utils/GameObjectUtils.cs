﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Utils
{
	public static class GameObjectUtils
	{
		public static GameObject Instantiate (this UnityEngine.Object objectL, string name, UnityEngine.GameObject parent, bool retainLocals, bool makeItActive)
		{
			GameObject go = GameObject.Instantiate(objectL) as GameObject;
			if (parent != null)
			{
				go.AssignAsChildTo(parent, retainLocals);
			}

			go.name = name;
			go.SetActive(makeItActive);
			return go;
		}

		public static void AssignAsChildTo(this UnityEngine.GameObject childGameObject, UnityEngine.GameObject parentGameObject, bool retainLocals = false)
		{
			TransformUtils.AssignAsParent(parentGameObject.transform, childGameObject.transform, retainLocals);
		}
	}
}
