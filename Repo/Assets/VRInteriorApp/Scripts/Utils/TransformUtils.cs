﻿using System;
using UnityEngine;
using System.Collections;

namespace Utils
{
	public static class TransformUtils
	{
		public static void AssignAsParent(Transform parent, Transform child, bool retainLocalTranform = false)
		{
			Vector3 position = child.localPosition;
			Quaternion rotation = child.localRotation;
			Vector3 scale = child.localScale;

			child.SetParent(parent);

			if (retainLocalTranform)
			{
				child.localPosition = position;
				child.localRotation = rotation;
				child.localScale = scale;
			}
		}

		public static void CopyTransform (this UnityEngine.Transform transform, UnityEngine.Transform refTransform, bool retainLocalTranform = false)
		{
			transform.position = refTransform.position;
			transform.rotation = refTransform.rotation;

			if (retainLocalTranform)
			{
				transform.localPosition = refTransform.localPosition;
				transform.localRotation = refTransform.localRotation;
			}

			transform.localScale = refTransform.localScale;
		}
	}
}
