﻿using UnityEngine;
using System.Collections;

public class DisableSameMesh : MonoBehaviour {

	public bool startSearch;
	public bool startDisable;
	public bool enableOriginal;
	public bool clear;
	public GameObject parentObject;

	[SerializeField] private MeshRenderer[] m_DuplicateObject;
	[SerializeField] private MeshRenderer[] m_OriginalObject;

	public void OnDrawGizmosSelected ()
	{
		if (startSearch == true)
		{
			startSearch = false;
			m_DuplicateObject = GetComponentsInChildren <MeshRenderer> ();
			m_OriginalObject = new MeshRenderer[m_DuplicateObject.Length];
			for (int i=0; i<m_DuplicateObject.Length; i++)
			{
				if (parentObject != null)
				{
					MeshRenderer[] tempObject = parentObject.GetComponentsInChildren<MeshRenderer> ();
					for (int j = 0; j < tempObject.Length; j++)
					{
						if (tempObject[j].name == m_DuplicateObject[i].name)
						{
							m_OriginalObject [i] = tempObject [j];
						}
					}
				}
			}
		}

		if (startDisable == true)
		{
			startDisable = false;
			for (int i = 0; i < m_OriginalObject.Length; i++)
			{
				m_DuplicateObject [i].enabled = true;
				m_OriginalObject [i].enabled = false;
			}
		}

		if (enableOriginal == true)
		{
			enableOriginal = false;
			for (int i = 0; i < m_OriginalObject.Length; i++)
			{
				m_OriginalObject [i].enabled = true;
				m_DuplicateObject [i].enabled = false;
			}
		}

		if (clear == true)
		{
			clear = false;
			m_DuplicateObject = new MeshRenderer[0];
			m_OriginalObject = new MeshRenderer[0];
		}
	}
}
