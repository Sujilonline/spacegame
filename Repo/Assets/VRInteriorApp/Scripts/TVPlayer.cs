﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TVPlayer : MonoBehaviour
{
	public Button[] testButton;

	// Use this for initialization
	void Start ()
	{
		for (int i = 0; i < testButton.Length; i++)
		{
			testButton [i].onClick.AddListener (() => { ButtonClick(testButton[i].name); });
		}
	}

	void ButtonClick (string name)
	{
		Debug.Log (name);
	}
}
