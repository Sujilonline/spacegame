﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Util.Objectpool;

public class NavigationArrowController : MonoBehaviour
{
	public float arrowSpeed = 3f;
	public GameObject arrowObject;
	//[HideInInspector]
	public Transform target;
	[HideInInspector]
	public Transform parentObject;
	[SerializeField] private List<NavigationArrow> m_arrowList = new List<NavigationArrow> ();

	private float elapsedTime;
	private NavMeshPath m_path;
	private NavigationArrow m_tempArrow;
	private bool m_canUpdate;

	public static NavigationArrowController instance { get; private set; }

	private void Awake()
	{
		// Only allow one instance at runtime.
//		if (instance != null)
//		{
//			enabled = false;
//			DestroyImmediate(this);
//			return;
//		}

		instance = this;
	}

	public void SetInstance()
	{
		instance = this;
	}

	// Use this for initialization
	void Start ()
	{
		m_path = new NavMeshPath ();
		GameObject go = new GameObject ("Arrow_Parent");
		parentObject = go.transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (m_canUpdate == false)
			return;

		elapsedTime += Time.deltaTime;
		if (target != null && elapsedTime > 1f)
		{
			elapsedTime -= 1f;
			Debug.Log(target.position);
			NavMesh.CalculatePath (transform.position, target.position, NavMesh.AllAreas, m_path);
		}

		if (m_arrowList.Count == 1 && target != null)
		{
			if (Vector3.Distance (transform.position, target.position) < 1f)
			{
				RemoveArrow ();
			}
		}

		for (int i = 0; i < m_path.corners.Length - 1; i++) {
			Debug.DrawLine (m_path.corners [i], m_path.corners [i + 1], Color.red);
		}

		if (m_arrowList.Count > 0 && (m_path.corners.Length -1) != m_arrowList.Count)
		{
			for (int i = 0; i < m_arrowList.Count; i++)
			{
				m_arrowList [i].DestroyObject ();
			}
			m_arrowList.Clear ();
			return;
		}

		if (m_path.corners.Length > 0 && m_arrowList.Count == 0)
		{
			for (int i = 0; i < m_path.corners.Length - 1; i++)
			{
				m_tempArrow = new NavigationArrow (m_path.corners [i], m_path.corners [i + 1], this);
				m_arrowList.Add (m_tempArrow);
			}
			return;
		}

		for (int i = 0; i < m_arrowList.Count; i++)
		{
			m_arrowList [i].Update ();
		}
	}

	public void AssignTarget (Transform _target)
	{
		target = _target;
		m_canUpdate = true;
	}

	public void RemoveArrow ()
	{
		m_canUpdate = false;
		target = null;
		for (int i = 0; i < m_arrowList.Count; i++)
		{
			m_arrowList [i].DestroyObject ();
		}
		m_arrowList.Clear ();
	}

	[System.Serializable]
	public class NavigationArrow
	{
		public float distance;
		public GameObject currentArrow;
		private Vector3 startPosition;
		private Vector3 endPosition;
		private NavigationArrowController arrowController;
		private float m_timespan;
		private bool canMove;

		public NavigationArrow (Vector3 startPos, Vector3 endPos, NavigationArrowController arrowControl)
		{
			arrowController = arrowControl;
			currentArrow = arrowController.arrowObject.Spawn(arrowController.parentObject);
			currentArrow.SetActive(false);
			startPos.y += 0.3f; 
			startPosition = startPos;
			endPos.y += 0.3f; 
			endPosition = endPos;
			currentArrow.transform.position = startPosition;
			currentArrow.transform.LookAt (endPosition);
			distance = Vector3.Distance(startPosition,endPosition);
		}

		public void Update ()
		{
			if (distance < 1f)
				return;

			if (canMove = true)
			{
				if (Vector3.Distance(currentArrow.transform.position, endPosition) <= 0)
				{
					currentArrow.gameObject.SetActive (false);
					currentArrow.transform.position = startPosition;
					canMove = false;
					m_timespan = Time.time;
					return;
				}

				if (currentArrow.activeSelf == false)
					currentArrow.SetActive (true);

				currentArrow.transform.position = Vector3.MoveTowards (currentArrow.transform.position, endPosition, (arrowController.arrowSpeed * Time.deltaTime));
			}
			else
			{
				if (Time.time + (Vector3.Distance (startPosition, endPosition) / 4f) > m_timespan)
					canMove = true;
			}
		}

		public void DestroyObject ()
		{
			currentArrow.Recycle ();
		}
	}
}
