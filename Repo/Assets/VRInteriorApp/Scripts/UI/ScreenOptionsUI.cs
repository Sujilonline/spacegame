﻿using UnityEngine;
using System.Collections;

public class ScreenOptionsUI : MonoBehaviour 
{

	public GameObject helpPanel;

	public void OnClickMainMenuButton ()
	{
		VRInteriorManager.instance.vrTouchControl.DoubleClick ();
	}

	public void DisplayHelpPanel()
	{
		if(helpPanel.activeSelf)
		{
			helpPanel.SetActive(false);
		}
		else
		{
			helpPanel.SetActive(true);
		}
	}

}
