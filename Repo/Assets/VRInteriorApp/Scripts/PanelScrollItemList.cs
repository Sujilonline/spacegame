﻿using UnityEngine;
using System.Collections;

public class PanelScrollItemList : MonoBehaviour 
{
	[Header ("SCROLL CONTENT")]
	public RectTransform[] panelScrolItem;
	public bool isTeleport;

	public GameObject leftObject;
	public GameObject centreObject;
	public GameObject rightObject;
}
