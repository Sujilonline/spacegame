﻿using UnityEngine;
using System.Collections;

public class HouseMap : MonoBehaviour {

	public Transform player;
	public Transform previousPosition;
	public GameObject groundFloor;
	public GameObject firstFloor;

	public GameObject mapWithoutPositionGroundFloor;
	public GameObject mapWithoutPositionFirstFloor;

	public GameObject mapWithPositionPanel;
	public GameObject mapWithoutPositionPanel;

	[SerializeField] private Transform m_playerDummy;

	private Vector3 m_temp_Position;

	void OnEnable ()
	{
		
	}

	void Update () {
	
		m_temp_Position = player.position;
		m_temp_Position.x /= transform.localScale.x;
		m_temp_Position.z /= transform.localScale.z;
		m_temp_Position.y = m_playerDummy.localPosition.y;
		m_playerDummy.localPosition = m_temp_Position;

	}

	public void SavePreviousPosition (Vector3 position, bool _firstfloor)
	{
		Debug.Log ("SavePreviousPosition "+position+" ::: "+_firstfloor);
		Vector3 temp = position;
		temp.x /= transform.localScale.x;
		temp.z /= transform.localScale.z;
		temp.y = previousPosition.localPosition.y;
		previousPosition.localPosition = temp;

		firstFloor.SetActive (_firstfloor);
		mapWithoutPositionFirstFloor.SetActive(!_firstfloor);
	}

	public GameObject GetActiveMapPanel()
	{
		if(mapWithPositionPanel.activeSelf)
		{
			return mapWithPositionPanel;
		}
		else
		{
			return mapWithoutPositionPanel;
		}
	}

	public GameObject GetInActiveMapPanel()
	{
		if(mapWithPositionPanel.activeSelf)
		{
			return mapWithoutPositionPanel;
		}
		else
		{
			return mapWithPositionPanel;
		}
	}

	public void SetPositionActive(bool setPositionActive)
	{
		if(setPositionActive)
		{
			if(mapWithPositionPanel.activeSelf)
			{
				previousPosition.gameObject.SetActive(true);
			}
			else
			{
				previousPosition.gameObject.SetActive(false);
			}
		}
		else
		{
			previousPosition.gameObject.SetActive(false);
		}
	}
}
