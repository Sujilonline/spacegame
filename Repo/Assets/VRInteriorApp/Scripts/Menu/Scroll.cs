﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class Scroll : MonoBehaviour 
{
	public HouseMap miniMap;


	[Header ("VERTICAL SCROLL CONTENT")]
	public RectTransform[] panelVerticalScrolItem;

	[Header ("VERTICAL SCROLL POSITION")]
	public Vector3 verticalTopPosition;
	public Vector3 verticalCenterPoistion;
	public Vector3 verticalBottomPosition;

	[Header ("SCROLL CONTENT")]
	public RectTransform[] panelScrolItem;

	[Header ("UI SCROLL POSITION")]
	public Vector3 leftPosition;
	public Vector3 centerPosition;
	public Vector3 rightPosition;
	public Vector3 leftMostPosition;
	public Vector3 rightMostPosition;

	[Header ("MAP UI SCROLL POSITION")]
	public Vector3 centerMapPosition;
	public Vector3 leftMostMapPosition;
	public Vector3 rightMostMapPosition;

	[SerializeField] private GameObject m_leftObject;
	[SerializeField] private GameObject m_centerObject;
	[SerializeField] private GameObject m_rightObject;

	[SerializeField] private GameObject m_verticalTopObject;
	[SerializeField] private GameObject m_verticalCenterObject;
	[SerializeField] private GameObject m_verticalBottomObject;

	public bool p_isSwipable
	{
		get { return m_isSwipable; }
		set { m_isSwipable = value; }
	}

	public bool p_isTeleport
	{
		get { return m_isTeleport; }
		set { m_isTeleport = value; }
	}

	private bool m_isSwipable = true;

	private bool m_isTeleport = true;

	void OnEnable ()
	{
		m_isTeleport = true;
		ResetVerticalScrolPanel();
	}

	public void ResetScrolPanel ()
	{
		for ( int i = 0; i < panelScrolItem.Length; i++)
		{
			if(panelScrolItem [i].GetComponent<iTween>() != null)
			{
				Destroy(panelScrolItem [i].GetComponent<iTween>());
			}
			panelScrolItem [i].gameObject.SetActive (false);
			panelScrolItem [i].GetComponent<Button> ().enabled = false;
		}

		panelScrolItem [panelScrolItem.Length - 1].anchoredPosition3D = leftPosition;
		m_leftObject = panelScrolItem [panelScrolItem.Length - 1].gameObject;
		m_leftObject.GetComponent<Button> ().enabled = true;
		panelScrolItem [0].anchoredPosition3D = centerPosition;
		m_centerObject = panelScrolItem [0].gameObject;
		m_centerObject.GetComponent<Button> ().enabled = true;
		panelScrolItem [1].anchoredPosition3D = rightPosition;
		m_rightObject = panelScrolItem [1].gameObject;
		m_rightObject.GetComponent<Button> ().enabled = true;

		m_leftObject.SetActive (true);
		m_leftObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		m_centerObject.SetActive (true);
		m_centerObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		m_rightObject.SetActive (true);
		m_rightObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);

		m_isSwipable = true;

	}


	public void ResetScrolPanelDuringSwipe ()
	{
		for ( int i = 0; i < panelScrolItem.Length; i++)
		{
			if(panelScrolItem [i].GetComponent<iTween>() != null)
			{
				Destroy(panelScrolItem [i].GetComponent<iTween>());
			}
			panelScrolItem [i].gameObject.SetActive (false);
			panelScrolItem [i].GetComponent<Button> ().enabled = false;
		}

		((RectTransform)m_leftObject.transform).anchoredPosition3D = leftPosition;
		m_leftObject.GetComponent<Button> ().enabled = true;
		((RectTransform)m_centerObject.transform).anchoredPosition3D = centerPosition;
		m_centerObject.GetComponent<Button> ().enabled = true;
		((RectTransform)m_rightObject.transform).anchoredPosition3D = rightPosition;
		m_rightObject.GetComponent<Button> ().enabled = true;

		m_leftObject.SetActive (true);
		m_leftObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		m_centerObject.SetActive (true);
		m_centerObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		m_rightObject.SetActive (true);
		m_rightObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
	}

	public void ResetVerticalScrolPanel ()
	{
		for ( int i = 0; i < panelVerticalScrolItem.Length; i++)
		{
			if(panelVerticalScrolItem [i].GetComponent<iTween>() != null)
			{
				Destroy(panelVerticalScrolItem [i].GetComponent<iTween>());
			}
			panelVerticalScrolItem [i].gameObject.SetActive (false);
		}

		panelVerticalScrolItem [panelVerticalScrolItem.Length - 1].anchoredPosition3D = verticalTopPosition;
		m_verticalTopObject = panelVerticalScrolItem [panelVerticalScrolItem.Length - 1].gameObject;
		panelVerticalScrolItem [0].anchoredPosition3D = verticalCenterPoistion;
		m_verticalCenterObject = panelVerticalScrolItem [0].gameObject;
		panelVerticalScrolItem [1].anchoredPosition3D = verticalBottomPosition;
		m_verticalBottomObject = panelVerticalScrolItem [1].gameObject;

		m_verticalTopObject.SetActive (false);
		m_verticalCenterObject.SetActive (true);
		m_verticalBottomObject.SetActive (false);

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") != null)
		{
			panelScrolItem = m_verticalCenterObject.GetComponent<PanelScrollItemList>().panelScrolItem;
			m_isTeleport = m_verticalCenterObject.GetComponent<PanelScrollItemList>().isTeleport;
			ResetScrolPanel();
		}
		else
		{
			m_isTeleport = true;
		}
	}

	public void FinishTween()
	{
		iTween.Stop();

		for ( int i = 0; i < panelScrolItem.Length; i++)
		{
			if(panelScrolItem [i].GetComponent<iTween>() != null)
			{
				Destroy(panelScrolItem [i].GetComponent<iTween>());
			}
			panelScrolItem [i].gameObject.SetActive (false);
		}

		for ( int i = 0; i < panelVerticalScrolItem.Length; i++)
		{
			if(panelVerticalScrolItem [i].GetComponent<iTween>() != null)
			{
				Destroy(panelVerticalScrolItem [i].GetComponent<iTween>());
			}
			panelVerticalScrolItem [i].gameObject.SetActive (false);
		}

		((RectTransform)m_leftObject.transform).anchoredPosition3D = leftPosition;
		m_leftObject.SetActive(true);
		m_leftObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		((RectTransform)m_centerObject.transform).anchoredPosition3D = centerPosition;
		m_centerObject.SetActive(true);
		m_centerObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		((RectTransform)m_rightObject.transform).anchoredPosition3D = rightPosition;
		m_rightObject.SetActive(true);
		m_rightObject.GetComponent<VRInteriorUIFader>().ChangeColor(1f);

		((RectTransform)m_verticalTopObject.transform).anchoredPosition3D = verticalTopPosition;
		m_verticalTopObject.SetActive(false);
		((RectTransform)m_verticalCenterObject.transform).anchoredPosition3D = verticalCenterPoistion;
		m_verticalCenterObject.SetActive(true);
		((RectTransform)m_verticalBottomObject.transform).anchoredPosition3D = verticalBottomPosition;
		m_verticalBottomObject.SetActive(false);


		ResetMap();
	}

	RectTransform GetNextObject (GameObject gob, bool scrollLeft)
	{
		if (scrollLeft == true)
		{
			for (int i = 0; i < panelScrolItem.Length; i++)
			{
				if ( panelScrolItem[i].gameObject == gob )
				{
					if (i == panelScrolItem.Length - 1)
						return panelScrolItem [0];
					else
						return panelScrolItem [i + 1];
				}
			}
		}
		else
		{
			for (int i = panelScrolItem.Length - 1; i >= 0; i--)
			{
				if ( panelScrolItem[i].gameObject == gob )
				{
					if (i == 0)
						return panelScrolItem [panelScrolItem.Length - 1];
					else
						return panelScrolItem [i - 1];
				}
			}
		}

		return null;
	}


	RectTransform GetNextVerticalObject (GameObject gob, bool scrollUp)
	{
		if (scrollUp == true)
		{
			for (int i = 0; i < panelVerticalScrolItem.Length; i++)
			{
				if ( panelVerticalScrolItem[i].gameObject == gob )
				{
					if (i == panelVerticalScrolItem.Length - 1)
						return panelVerticalScrolItem [0];
					else
						return panelVerticalScrolItem [i + 1];
				}
			}
		}
		else
		{
			for (int i = panelVerticalScrolItem.Length - 1; i >= 0; i--)
			{
				if ( panelVerticalScrolItem[i].gameObject == gob )
				{
					if (i == 0)
						return panelVerticalScrolItem [panelVerticalScrolItem.Length - 1];
					else
						return panelVerticalScrolItem [i - 1];
				}
			}
		}

		return null;
	}

	public void SwipeLeft ()
	{

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") == null)
		{
			LeftSwipeMap();
			return;
		}

		MoveToLeftMost (m_leftObject);
		m_leftObject = null;

		MoveToLeft (m_centerObject);
		m_leftObject = m_centerObject;
		m_leftObject.GetComponent<Button> ().enabled = true;
		m_centerObject = null;
		Debug.Log("left "+m_leftObject);

		MoveToCenter (m_rightObject);
		m_centerObject = m_rightObject;
		m_centerObject.GetComponent<Button> ().enabled = true;
		m_rightObject = null;
		Debug.Log("center "+m_centerObject);

		RectTransform rectObject = GetNextObject (m_centerObject, true);
		rectObject.anchoredPosition3D = rightMostPosition;
		MoveToRight (rectObject.gameObject);
		m_rightObject = rectObject.gameObject;
		m_rightObject.GetComponent<Button> ().enabled = true;
		Debug.Log("right "+m_rightObject);
	}

	public void SwipeRight ()
	{
		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") == null)
		{
			RightSwipeMap();
			return;
		}
		
		MoveToRightMost (m_rightObject);
		m_rightObject = null;
		
		MoveToRight (m_centerObject);
		m_rightObject = m_centerObject;
		m_rightObject.GetComponent<Button> ().enabled = true;
		m_centerObject = null;
		
		MoveToCenter (m_leftObject);
		m_centerObject = m_leftObject;
		m_centerObject.GetComponent<Button> ().enabled = true;
		m_leftObject = null;
		
		RectTransform rectObject = GetNextObject (m_centerObject, false);
		rectObject.anchoredPosition3D = leftMostPosition;
		MoveToLeft (rectObject.gameObject);
		m_leftObject = rectObject.gameObject;
		m_leftObject.GetComponent<Button> ().enabled = true;
	}

	public void SwipeUp ()
	{
		PanelScrollItemList scrollItemList = new PanelScrollItemList();

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") != null)
		{
			scrollItemList = m_verticalCenterObject.GetComponent<PanelScrollItemList>();

			scrollItemList.centreObject = m_centerObject;
			scrollItemList.leftObject = m_leftObject;
			scrollItemList.rightObject = m_rightObject;
		}

		m_leftObject.GetComponent<Button> ().enabled = false;
		m_centerObject.GetComponent<Button> ().enabled = false;
		m_rightObject.GetComponent<Button> ().enabled = false;

		VerticalMoveToBottom (m_verticalCenterObject);
		m_verticalBottomObject = m_verticalCenterObject;
		m_verticalCenterObject = null;

		VerticalMoveToCentre (m_verticalTopObject);
		m_verticalCenterObject = m_verticalTopObject;
		m_verticalTopObject = null;

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") != null)
		{
			scrollItemList = m_verticalCenterObject.GetComponent<PanelScrollItemList>();

			panelScrolItem = scrollItemList.panelScrolItem;
			m_isTeleport = scrollItemList.isTeleport;

			m_centerObject = scrollItemList.centreObject;
			m_leftObject = scrollItemList.leftObject;
			m_rightObject = scrollItemList.rightObject;

			ResetScrolPanelDuringSwipe();
		}
		else
		{
			m_isTeleport = true;
		}

		RectTransform rectObject = GetNextVerticalObject (m_verticalCenterObject, false);
		rectObject.anchoredPosition3D = verticalTopPosition;
		VerticalMoveToTopMost (rectObject.gameObject);
		m_verticalTopObject = rectObject.gameObject;

	}

	public void SwipeDown ()
	{
		PanelScrollItemList scrollItemList = new PanelScrollItemList();

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") != null)
		{
			scrollItemList = m_verticalCenterObject.GetComponent<PanelScrollItemList>();

			scrollItemList.centreObject = m_centerObject;
			scrollItemList.leftObject = m_leftObject;
			scrollItemList.rightObject = m_rightObject;
		}

		m_leftObject.GetComponent<Button> ().enabled = false;
		m_centerObject.GetComponent<Button> ().enabled = false;
		m_rightObject.GetComponent<Button> ().enabled = false;

		VerticalMoveToTop (m_verticalCenterObject);
		m_verticalTopObject = m_verticalCenterObject;
		m_verticalCenterObject = null;

		VerticalMoveToCentre (m_verticalBottomObject);
		m_verticalCenterObject = m_verticalBottomObject;
		m_verticalBottomObject = null;

		if(m_verticalCenterObject.GetComponent("PanelScrollItemList") != null)
		{
			scrollItemList = m_verticalCenterObject.GetComponent<PanelScrollItemList>();

			panelScrolItem = scrollItemList.panelScrolItem;
			m_isTeleport = scrollItemList.isTeleport;

			m_centerObject = scrollItemList.centreObject;
			m_leftObject = scrollItemList.leftObject;
			m_rightObject = scrollItemList.rightObject;

			ResetScrolPanelDuringSwipe();
		}
		else
		{
			m_isTeleport = true;
		}

		RectTransform rectObject = GetNextVerticalObject (m_verticalCenterObject, true);
		rectObject.anchoredPosition3D = verticalBottomPosition;
		VerticalMoveToBottomMost (rectObject.gameObject);
		m_verticalBottomObject = rectObject.gameObject;
	}


	#region ITWEEN

	void VerticalMoveToTopMost (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = verticalTopPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();

		gob.SetActive (false);
	}

	void VerticalMoveToBottomMost (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = verticalBottomPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();

		gob.SetActive (false);
	}
	void VerticalMoveToTop (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = verticalTopPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedVerticalTop";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}

	void ReachedVerticalTop (GameObject gob)
	{
//		iTween.Stop();
		gob.SetActive (false);
		m_isSwipable = true;
	}

	void VerticalMoveToCentre (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = verticalCenterPoistion;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeInFull();
	}

	void VerticalMoveToBottom (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = verticalBottomPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedVerticalBottom";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}

	void ReachedVerticalBottom (GameObject gob)
	{
//		iTween.Stop();
		gob.SetActive (false);
		m_isSwipable = true;
	}

	void MoveToCenter (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = centerPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeInHalfToFull();
	}

	void MoveToLeftMostMap (GameObject gob)
	{
		miniMap.SetPositionActive(false);

		Hashtable newHash = new Hashtable ();
		newHash ["position"] = leftMostMapPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedLeftMost";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeOutHalfToFull();
		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}

	void MoveToCenterForMap (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = centerMapPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		gob.GetComponent<VRInteriorUIFader>().FadeInFull();
	}

	void MoveToRightMostMap (GameObject gob)
	{
		miniMap.SetPositionActive(false);

		Hashtable newHash = new Hashtable ();
		newHash ["position"] = rightMostMapPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedRightMost";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeOutHalfToFull();
		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}

	void MoveToLeft (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = leftPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;
		newHash ["islocal"] = true;

		if (gob.activeSelf == true)
		{
			//			gob.GetComponent<VRInteriorUIFader>().FadeOutHalf();
		}
		else
		{
			gob.SetActive (true);
			//			gob.GetComponent<VRInteriorUIFader>().FadeInHalf();
			gob.GetComponent<VRInteriorUIFader>().FadeInFull();
		}

		iTween.MoveTo (gob, newHash);

	}

	void MoveToRight (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = rightPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;

		if (gob.activeSelf == true)
		{
			//			gob.GetComponent<VRInteriorUIFader>().FadeOutHalf();
		}
		else
		{
			gob.SetActive (true);
			//			gob.GetComponent<VRInteriorUIFader>().FadeInHalf();
			gob.GetComponent<VRInteriorUIFader>().FadeInFull();
		}

		iTween.MoveTo (gob, newHash);

	}

	void MoveToLeftMost (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = leftMostPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedLeftMost";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeOutHalfToFull();
		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}

	void ReachedLeftMost (GameObject gob)
	{
//		iTween.Stop();
		gob.SetActive (false);
		m_isSwipable = true;
		miniMap.SetPositionActive(true);
	}

	void MoveToRightMost (GameObject gob)
	{
		Hashtable newHash = new Hashtable ();
		newHash ["position"] = rightMostPosition;
		newHash ["easeType"] = iTween.EaseType.easeInOutExpo;   
		newHash ["islocal"] = true;
		newHash ["oncomplete"] = "ReachedRightMost";
		newHash ["oncompletetarget"] = gameObject;
		newHash ["oncompleteparams"] = gob;

		gob.SetActive (true);
		iTween.MoveTo (gob, newHash);

		//		gob.GetComponent<VRInteriorUIFader>().FadeOutHalfToFull();
		gob.GetComponent<VRInteriorUIFader>().FadeOutFull();
	}

	void ReachedRightMost (GameObject gob)
	{
//		iTween.Stop();
		gob.SetActive (false);
		m_isSwipable = true;
		miniMap.SetPositionActive(true);
	}

	#endregion

	public void LeftSwipeMap()
	{
		MoveToLeftMostMap(miniMap.GetActiveMapPanel());

		GameObject tempObject = miniMap.GetInActiveMapPanel();

		((RectTransform)tempObject.transform).anchoredPosition3D = rightMostMapPosition;
		MoveToCenterForMap (tempObject);
	}

	public void RightSwipeMap()
	{
		MoveToRightMostMap(miniMap.GetActiveMapPanel());

		GameObject tempObject = miniMap.GetInActiveMapPanel();

		((RectTransform)tempObject.transform).anchoredPosition3D = leftMostMapPosition;
		MoveToCenterForMap (tempObject);
	}

	public void ResetMap()
	{
		if(miniMap.mapWithoutPositionPanel.GetComponent<iTween>() != null)
		{
			Destroy(miniMap.mapWithoutPositionPanel.GetComponent<iTween>());
		}
		if(miniMap.mapWithPositionPanel.GetComponent<iTween>() != null)
		{
			Destroy(miniMap.mapWithPositionPanel.GetComponent<iTween>());
		}
		((RectTransform)miniMap.mapWithoutPositionPanel.transform).anchoredPosition3D = leftMostMapPosition;
		miniMap.mapWithoutPositionPanel.SetActive(false);
		((RectTransform)miniMap.mapWithPositionPanel.transform).anchoredPosition3D = centerMapPosition;
		miniMap.mapWithPositionPanel.SetActive(true);
		miniMap.mapWithPositionPanel.GetComponent<VRInteriorUIFader>().ChangeColor(1f);
		miniMap.SetPositionActive(true);
	}
}
