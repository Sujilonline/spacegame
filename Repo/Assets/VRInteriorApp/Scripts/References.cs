﻿using UnityEngine;
using System.Collections;
using Utils;

public class References : MonoBehaviour {

	public string playerMobile, playerVR, playerWeb, joystickControls;
	public GameObject VRInteriorManager;
	public GameObject mainHUD;
	public GameObject mainHUDMOBILE;
	public GameObject menuUI;

	public GameObject GetPlayer ()
	{
		GameObject _Player = null;

		#if !UNITY_VR && (UNITY_IOS || UNITY_ANDROID)
		_Player = Resources.Load(playerMobile) as GameObject;
		#elif UNITY_WEBGL
		_Player = Resources.Load(playerWeb) as GameObject;
		#else
		_Player = Resources.Load(playerVR) as GameObject;
		#endif
				
		return _Player;
	}

	public GameObject GetMobileJoySticks ()
	{
		return Resources.Load (joystickControls) as GameObject;
	}
}
