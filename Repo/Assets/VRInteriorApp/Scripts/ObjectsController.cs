﻿using UnityEngine;
using System.Collections;

public class ObjectsController : MonoBehaviour
{
	public GameObject[] visibleObjects;
	public GameObject[] unSeenObjects;

	public bool showObjects = true;
	public bool triggerFirstFloor;
	public bool triggerGroundFloor;

	void Awake ()
	{
		if (visibleObjects == null || visibleObjects.Length == 0)
			return;

		for (int i = 0; i < visibleObjects.Length; i++)
		{
			visibleObjects [i].GetComponent<GameObjectController>().ActiveGameObject(false);
		}
	}
	
	void OnTriggerEnter (Collider others)
	{
		if (others.tag == "Player")
		{
			ObjectControl ();
		}
	}

	public void ObjectControl ()
	{
		if (triggerFirstFloor == true)
			VRInteriorManager.instance.vrTouchControl.isInFirstFloor = true;

		if (triggerGroundFloor == true)
			VRInteriorManager.instance.vrTouchControl.isInFirstFloor = false;

		if (visibleObjects == null || visibleObjects.Length == 0)
			return;

		for (int i = 0; i < visibleObjects.Length; i++)
		{
			if (visibleObjects[i] != null)
				visibleObjects [i].GetComponent<GameObjectController>().ActiveGameObject (showObjects);
		}

		if (unSeenObjects == null || unSeenObjects.Length == 0)
			return;

		for (int i = 0; i < unSeenObjects.Length; i++)
		{
			if (unSeenObjects[i] != null)
				unSeenObjects [i].GetComponent<GameObjectController>().ActiveGameObject (!showObjects);
		}
	}
}
