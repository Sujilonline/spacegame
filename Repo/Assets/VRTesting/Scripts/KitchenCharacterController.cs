﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using VRStandardAssets.Maze;
using VRStandardAssets.Utils;
using UnityEngine.SceneManagement;

public class KitchenCharacterController : MonoBehaviour {

	[SerializeField] private MazeTargetSetting[] m_MazeTargetSetting;
	[SerializeField] private SelectionSlider m_SelectionSlider;

	public NavMeshAgent agent { get; private set; }             // Navmesh agent required for the path finding
		
    [SerializeField] private Vector3 m_TargetPosition;

	private void OnEnable ()
	{
		foreach(MazeTargetSetting target in m_MazeTargetSetting)
		{
			target.OnTargetSet += HandleSetTarget;
			target.Activate();
		}

		m_SelectionSlider.OnBarFilled += HandleBarFilled;
	}
	
	
	private void OnDisable()
	{
		foreach (MazeTargetSetting target in m_MazeTargetSetting)
		{
			target.OnTargetSet -= HandleSetTarget;
		}

		m_SelectionSlider.OnBarFilled -= HandleBarFilled;
	}
	
	private void Start()
	{
		// get the components on the object we need ( should not be null due to require component so no need to check )
		agent = GetComponentInChildren<NavMeshAgent>();
		
		agent.updateRotation = false;
		agent.updatePosition = true;

		if (VRDeviceManager.Instance.playerPosition != Vector3.zero)
		{
			transform.position = VRDeviceManager.Instance.playerPosition;
			transform.rotation = VRDeviceManager.Instance.playerRotation;
		}

		m_TargetPosition = transform.position;
	}
	
	
	private void Update()
	{
		agent.SetDestination(m_TargetPosition);

		if (QualitySettings.antiAliasing != 8)
			QualitySettings.antiAliasing = 8;
		
		if (agent.remainingDistance > agent.stoppingDistance)
		{
			//Move(agent.desiredVelocity);
		}
		else
		{
			//Move(Vector3.zero);
		}
	}

	void Move(Vector3 move)
	{
		if (move.magnitude > 1f) move.Normalize();
			move = transform.InverseTransformDirection(move);
	}

	private void HandleSetTarget(Transform target)
	{
		SetTarget(target.position);
	}

	private void HandleBarFilled()
	{
		VRDeviceManager.Instance.playerPosition = transform.position;
		VRDeviceManager.Instance.playerRotation = transform.rotation;

		if (SceneManager.GetActiveScene().name == "KitchenTest_2")
			SceneManager.LoadScene ("KitchenTest_3");
		else if (SceneManager.GetActiveScene().name == "KitchenTest_3")
			SceneManager.LoadScene ("KitchenTest_2");
	}
	
	void SetTarget(Vector3 targetPosition)
	{
		m_TargetPosition = targetPosition;
	}
    
    public RawImage rawimage;
    void WebCam()
    {
        WebCamTexture webcamTexture = new WebCamTexture();
        rawimage.texture = webcamTexture;
        rawimage.material.mainTexture = webcamTexture;
        webcamTexture.Play();
    }
}
