﻿using UnityEngine;
using System.Collections.Generic;

public class ColorChanger : MonoBehaviour 
{
	// Use this for initialization
	public List<Color> colors;
	void Start () 
	{
		this.GetComponent<MeshRenderer> ().material.color = colors [Random.Range (0, colors.Count)];
	}
	
}
