﻿using UnityEngine;
using System.Collections;

public class TestScript : MonoBehaviour 
{

	public GameObject playerMobile, playerVR;
	public GameObject playerobject;
	// Use this for initialization
	void Start () 
	{
		#if !UNITY_VR && (UNITY_IOS || UNITY_IPHONE || UNITY_ANDROID)
		playerobject = Instantiate (playerMobile, Vector3.zero, Quaternion.identity) as GameObject;
		#else
		playerobject = Instantiate (playerVR, Vector3.zero, Quaternion.identity) as GameObject;
		#endif
		playerobject.SetActive(true);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
