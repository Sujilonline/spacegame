// Upgrade NOTE: replaced 'glstate.matrix.mvp' with 'UNITY_MATRIX_MVP'

Shader "Hidden/Render Depth" {
#warning Upgrade NOTE: SubShader commented out; uses TRANSFER_EYEDEPTH/OUTPUT_EYEDEPTH. Those were changed to UNITY_TRANSFER_DEPTH/UNITY_OUTPUT_DEPTH, and the argument changed from float to float2. Update your shader code.
/*SubShader {
    Tags { "RenderType"="Opaque" }
    Pass {
        Fog { Mode Off }
        
CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

struct v2f {
    float4 pos : POSITION;
    float depth : TEXCOORD0;
};

v2f vert( appdata_base v ) {
    v2f o;
    o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
    TRANSFER_EYEDEPTH(o.depth);
    return o;
}

half4 frag(v2f i) : COLOR {
    OUTPUT_EYEDEPTH(i.depth);
}

ENDCG

    }
}*/

Fallback Off

}
